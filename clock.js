$(function () {
  const navbar = $("nav")[0];
  const clock = $("<div />", {
    text: "getting time...",
    id: "clock",
  }).appendTo(navbar);
  const date = $("<div />", {
    text: "getting date...",
    id: "date",
  }).appendTo(navbar);
  setInterval(() => {
    const time = getCurrentIjedaDateTime();
    const currentDate = getToday();
    clock.text(`${time.kemoka}:${time.helaka}:${time.žakemo}`);
    date.text(`${currentDate.join("/")}`);
  }, 10);
});

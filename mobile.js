function checkMobile() {
  if ($(window).width() < 799) {
    $("body").addClass("mobile");
    $("nav").fadeOut(100);
  } else {
    $("body").removeClass("mobile");
    $("nav").fadeIn(100);
  }
}

$(function () {
  $(window).on("resize", checkMobile);
  checkMobile();
});

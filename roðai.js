//date

const MONTHS = ["Seðysa", "Ysto", "Šomi", "Þaša", "Koža"];
const WEEKDAYS = [
  "Ukeð",
  "Tersy",
  "Naki",
  "Sosaš",
  "Zeža",
  "Kasað",
  "Mešto",
  "Yntaja",
  "Ðesoðo",
  "Sasað",
  "Þareið",
  "Retað",
];

const EPOCH = 2460025.5;

function getToday() {
  return fromJD(getJD());
}

function getJD() {
  return new Date() / 86400000 + 2440587.5;
}

function toJD(year, month, day) {
  return day + (month - 1) * 73 + year * 365 + Math.ceil(year / 4) + EPOCH - 1;
}

function fromJD(jdc) {
  cdc = Math.floor(jdc) + 0.5 - EPOCH;
  year = Math.floor((cdc - Math.ceil((cdc - 1) / 1461)) / 365);

  yday = jdc - toJD(year, 1, 1);

  month = Math.floor(yday / 73) + 1;
  day = yday - (month - 1) * 73 + 1;

  return [Math.floor(year), Math.floor(month), Math.floor(day)];
}

//clock

function getCurrentIjedaDateTime() {
  const currentUTC = new Date();
  const offset = -4; // UTC-4 timezone offset

  const localDate = new Date(currentUTC.getTime() + offset * 60 * 60 * 1000);

  const localSeconds =
    localDate.getUTCHours() * 3600 +
    localDate.getUTCMinutes() * 60 +
    localDate.getUTCSeconds();
  const žakemo = Math.floor(localSeconds / 1.728) % 50;
  const helaka = Math.floor(localSeconds / 86.4) % 100;
  const kemoka = Math.floor(localSeconds / 8640) % 10;

  return {
    kemoka: kemoka,
    helaka: helaka,
    žakemo: žakemo,
  };
}

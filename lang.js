function setlang(lang) {
  localStorage.setItem("lang", lang);
  $("html").attr("lang", lang);
  $(".langselector > *").removeClass("active");
  $(".langselector > .lang-" + lang).addClass("active");
}

$(function () {
  if (localStorage.getItem("lang") === null) {
    $(".langselector > .lang-eud").addClass("active");
  } else {
    var lang = localStorage.getItem("lang");
    $("html").attr("lang", lang);
    $(".langselector > .lang-" + lang).addClass("active");
  }
});
